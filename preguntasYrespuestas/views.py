from django.http import HttpResponse, Http404
from preguntasYrespuestas.models import Pregunta,Respuesta
from django.shortcuts import  get_object_or_404, render_to_response, redirect, render
from  django.contrib.auth.decorators import login_required
from  preguntasYrespuestas.form import PreguntaForm,RespuestaForm
from django.utils import timezone


def index(request):
    preguntas = Pregunta.objects.all()
    return render_to_response('preguntasYrespuestas/index.html',{'preguntas':preguntas})

def pregunta_detalle(request, pregunta_id):
    form = RespuestaForm()
    pregunta = get_object_or_404(Pregunta, pk=pregunta_id)
    if request.method == 'POST':
        respuesta = request.POST.get('contenido')
        pregunta.respuesta_set.create(contenido=respuesta)
        pregunta.save()
    return render(request, 'preguntasYrespuestas/pregunta_detalle.html',{'pregunta':pregunta,'form':form})

@login_required
def crear_pregunta(request):
    if request.method == 'POST':
        form = PreguntaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('preguntas')
    else:
        form = PreguntaForm()
    return render(request,'preguntasYrespuestas/crear_pregunta.html',{'form':form})

@login_required
def editar_pregunta(request, pregunta_id):
    pregunta = get_object_or_404(Pregunta, pk=pregunta_id)
    if request.method == 'POST':
        form = PreguntaForm(request.POST, instance=pregunta)
        if form.is_valid():
            form.save()
            return redirect('pregunta_detalle',pregunta_id)
    else:
        form = PreguntaForm(instance=pregunta)
    return render(request,'preguntasYrespuestas/editar_pregunta.html',{'form':form})