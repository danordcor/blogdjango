from django.conf.urls import url, include
from django.contrib import admin
from preguntasYrespuestas.views import index, pregunta_detalle, crear_pregunta, editar_pregunta
from proyecto1 import views
admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^response/(?P<pregunta_id>\d+)/$', pregunta_detalle, name='pregunta_detalle'),
    url(r'^response/$', index, name='preguntas'),
    url(r'^response/crear', crear_pregunta,name='crear_pregunta'),
    url(r'^response/editar/(?P<pregunta_id>\d+)/$', editar_pregunta,name='editar_pregunta'),
    url(r'^login/$', views.login_page, name='Login'),
    url(r'^logout/$', views.exit, name='Logout'),
    url(r'^$', views.home, name='home')
]
